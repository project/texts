<?php

namespace Drupal\texts;

use Drupal\Core\StringTranslation\TranslatableMarkup;

class TextsTranslatableMarkup extends TranslatableMarkup {

  /**
   * Renders the object as a string.
   *
   * @return string
   *   The translated string.
   */
  public function render() {
    if (!isset($this->translatedMarkup)) {
      $this->translatedMarkup = $this->string;
    }

    // Handle any replacements.
    if ($args = $this->getArguments()) {
      return $this->placeholderFormat($this->translatedMarkup, $args);
    }
    return $this->translatedMarkup;
  }

}
