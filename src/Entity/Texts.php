<?php

namespace Drupal\texts\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\texts\TextsInterface;

/**
 * Defines the string translation entity class.
 *
 * @ContentEntityType(
 *   id = "texts",
 *   label = @Translation("Texts"),
 *   label_collection = @Translation("Texts"),
 *   handlers = {
 *     "storage" = "Drupal\texts\TextsStorage",
 *     "storage_schema" = "Drupal\texts\TextsContentEntityStorageSchema",
 *     "view_builder" = "Drupal\texts\TextsViewBuilder",
 *     "list_builder" = "Drupal\texts\TextsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\texts\TextsAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\texts\Form\TextsForm",
 *       "edit" = "Drupal\texts\Form\TextsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\texts\TextsRouteProvider",
 *     }
 *   },
 *   base_table = "texts",
 *   data_table = "texts_field_data",
 *   translatable = TRUE,
 *   admin_permission = "access string translation overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "key",
 *     "uuid" = "uuid",
 *   },
 *   field_indexes = {
 *     "key"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/texts/{texts}/edit",
 *     "add-form" = "/admin/content/texts/add",
 *     "edit-form" = "/admin/content/texts/{texts}/edit",
 *     "delete-form" = "/admin/content/texts/{texts}/delete",
 *     "collection" = "/admin/content/texts"
 *   },
 * )
 */
class Texts extends ContentEntityBase implements TextsInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    return $this->get('context')->value ?? 'default';
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(string $context): TextsInterface {
    $this->set('context', $context);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationKey(): string {
    return $this->get('key')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setTranslationKey(string $key): TextsInterface {
    $this->set('key', $key);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPlural(): bool {
    return (bool) $this->get('plural')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlural(bool $plural): TextsInterface {
    $this->set('plural', $plural);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationText(): string {
    return $this->get('translation')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setTranslationText(string $text): TextsInterface {
    $this->set('translation', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['key'] = BaseFieldDefinition::create('string')
      ->setLabel('Translation Key')
      ->setRequired(TRUE)
      ->addConstraint('UniqueKey')
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['context'] = BaseFieldDefinition::create('list_string')
      ->setLabel('Context')
      ->setInitialValue('default')
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values_function' => '\Drupal\texts\TextsContext::getStaticContextOptions',
      ])
      ->setDefaultValue('default')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['translation'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Translation')
      ->setRequired(TRUE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['plural'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Plural'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the string translation was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the string translation was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function getListCacheTagsToInvalidate() {
    $tags = parent::getCacheTagsToInvalidate();
    if ($context = $this->getContext()) {
      $tags[] = 'texts_context:' . $context;
    }
    return $tags;
  }

}
