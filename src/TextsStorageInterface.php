<?php

namespace Drupal\texts;

interface TextsStorageInterface {

  /**
   * Load a text by key.
   *
   * @param $key
   *
   * @return \Drupal\texts\TextsInterface|null
   */
  public function loadByKey(string $key, string $context = 'default'): TextsInterface|null;

}
