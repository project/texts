<?php

namespace Drupal\texts;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the string translation entity type.
 */
class TextsContext  {

  use StringTranslationTrait;

  protected $entityTypeManager;
  protected $database;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, $database) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getContextOptions() {
    return $this::getStaticContextOptions();
  }

  /**
   * {@inheritdoc}
   */
  public static function getStaticContextOptions() {
    $options = ['default' => t('Default')];
    /** @var \Drupal\Core\Database\Connection $database */
    $database = \Drupal::service('database');

    $query = $database->select('texts_field_data', 't')
      ->fields('t', ['context'])
      ->groupBy('context');
    $results = $query->execute();
    foreach ($results as $result) {
      $options[$result->context] = $result->context;
    }
    return $options;
  }

}
