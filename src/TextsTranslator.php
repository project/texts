<?php

namespace Drupal\texts;

use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class TextsTranslator implements TextsTranslatorInterface {

  /**
   * @var \Drupal\texts\TextsStorage
   */
  protected $textsStorage;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->textsStorage = $entity_type_manager->getStorage('texts');
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function trans(string $key, array $args = [], array $options = [], mixed $text = NULL): string {

    $context = 'default';
    if (!empty($options['context'])) {
      $context = $options['context'];
    }

    // Load entity if non was provided.
    if ($text === NULL) {
      $text = $this->textsStorage->loadByKey($key, $context);
    }

    $current_language_code = $this->languageManager->getCurrentLanguage()->getId();
    $translation = '';

    if ($text) {
      if ($text->hasTranslation($current_language_code)) {
        $text = $text->getTranslation($current_language_code);
      }
      $translation = $text->getTranslationText();

      // Add new default translation if there is none.
      if (empty($translation) && $options['default_translation']) {
        $text->setTranslationText($options['default_translation']);
        $text->save();
      }
    }

    // Fallback to provided default translation.
    if (empty($text)) {
      $translation = $options['default_translation'] ?? '';

      // Add new text to database.
      $new_text_data = [
        'key' => $key,
        'context' => $context,
        'translation' => $translation,
      ];
      if (TextsTranslator::isPlural($translation)) {
        $new_text_data['plural'] = TRUE;
      }
      /** @var \Drupal\texts\TextsInterface $new_text */
      $new_text = $this->textsStorage->create($new_text_data);
      $new_text->save();

      // Return the newly added translation.
      $translation = $new_text->getTranslationText();
    }

    // Use TranslatableMarkup to replace all placeholders.
    if ($args) {
      $translatable_markup = new TextsTranslatableMarkup($translation, $args, $options);

      if (TextsTranslator::isPlural($translation)) {
        $plurals = TextsTranslator::getPluralsParts($translation);
        $translatable_markup = new TextsPluralTranslatableMarkup($options['count'], $plurals[0], $plurals[1], $args, $options);
      }
      $translation = $translatable_markup->__toString();
    }

    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function formatPlural(int $count, string $key, string $singular = '', string $plural = '', array $args = [], array $options = []): string {
    $options['default_translation'] = TextsTranslator::getPluralMerged(
      [$singular, $plural]
    );
    $options['count'] = $count;
    $translation = $this->trans($key, $args, $options);
    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function translateMultiple(array $translations, &$cache_tags = []): array {
    $key_list = [];
    $results = [];

    foreach ($translations as $translation) {
      $key_list[] = $translation['key'];
    }

    /** @var \Drupal\texts\Entity\Texts[] $existingTexts */
    $existingTexts = $this->textsStorage->loadMultipleByKey($key_list);
    foreach ($translations as $index => $translation) {
      $context = $translation['context'] ?? 'default';
      $key = $translation['key'];
      $arrayKey = $context . '.' . $key;
      $results[$arrayKey] = '';
      $options = [];
      if (!empty($translations[$index]['default'])) {
        $options['default_translation'] = $translations[$index]['default'];
      }

      $options['context'] = $context;
      $cachetag = 'texts_context:' . $options['context'];
      $cache_tags[$cachetag] = $cachetag;

      $args = [];
      if (!empty($translations[$index]['args'])) {
        $args = $translations[$index]['args'];
      }

      $existing = $existingTexts[$arrayKey] ?? NULL;

      if ($existing) {
        $results[$arrayKey] = $existing->getTranslationText();
        continue;
      }

      $results[$arrayKey] = $this->trans($key, $args, $options);
    }
    return $results;
  }

  public static function isPlural($string): bool {
    $is_plural = FALSE;
    if (strpos($string, PoItem::DELIMITER) !== FALSE) {
      $is_plural = TRUE;
    }
    return $is_plural;
  }

  /**
   * Check if the string is a plural string.
   *
   * @param $string
   *
   * @return array|string[]
   */
  public static function getPluralsParts($string) {
    $result = [$string, $string];
    if (TextsTranslator::isPlural($string)) {
      $result = explode(PoItem::DELIMITER, $string);
    }
    return $result;
  }

  /**
   * Merge plural string parts.
   *
   * @param $elements
   *
   * @return string
   */
  public static function getPluralMerged($elements) {
    return implode(PoItem::DELIMITER, $elements);
  }

}
