<?php

namespace Drupal\texts\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * A Twig extension for the Symfony translation component.
 */
class TextsExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('getTexts', 'getTexts', ['is_safe' => ['html']]),
      new TwigFilter('getTextsPlural', 'getTextsPlural', ['is_safe' => ['html']]),
    ];
  }

}
