<?php

namespace Drupal\texts;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the string translation entity type.
 */
class TextsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view string translation');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['edit string translation', 'administer string translation'],
          'OR'
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['delete string translation', 'administer string translation'],
          'OR'
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      ['create string translation', 'administer string translation'],
      'OR'
    );
  }

}
