<?php

namespace Drupal\texts\Form\Overview;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\texts\TextsContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the locale user interface translation form base.
 *
 * Provides methods for searching and filtering strings.
 */
abstract class TranslationFormBase extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $stringTranslationStorage;

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Filter values. Shared between objects that inherit this class.
   *
   * @var array|null
   */
  protected static $filterValues;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $texts_storage
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(EntityStorageInterface $texts_storage, StateInterface $state, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory) {
    $this->stringTranslationStorage = $texts_storage;
    $this->state = $state;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('texts'),
      $container->get('state'),
      $container->get('language_manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Builds a string search query and returns an array of string objects.
   *
   * @return \Drupal\texts\Entity\Texts[]
   *   Array of \Drupal\texts\Entity\Texts objects.
   */
  protected function translateFilterLoadStrings() {
    $filter_values = $this->translateFilterValues();

    $query = $this->stringTranslationStorage->getQuery();
    $query->accessCheck(FALSE);

    // Language is sanitized to be one of the possible options in
    // translateFilterValues().
    if (!empty($filter_values['langcode']) && $filter_values['langcode'] !== 'all') {
      $query->condition('langcode', $filter_values['langcode']);
    }

    if (!empty($filter_values['context']) && $filter_values['context'] !== 'all') {
      $query->condition('context', $filter_values['context']);
    }

    if (!empty($filter_values['string'])) {
      $or = $query->orConditionGroup();
      $or->condition('key', '%' . $filter_values['string'] . '%', 'LIKE');
      $or->condition('translation', '%' . $filter_values['string'] . '%', 'LIKE');
      $query->condition($or);
    }

    // Sort
    $query
      ->sort('changed', 'DESC')
      ->sort('id', 'DESC');

    // Limit
    $query = $query->pager('30');

    $ids = $query->execute();

    $result = [];

    if ($ids) {
      $result = $this->stringTranslationStorage->loadMultiple($ids);
    }

    return $result;
  }

  /**
   * Builds an array out of search criteria specified in request variables.
   *
   * @param bool $reset
   *   If the list of values should be reset.
   *
   * @return array
   *   The filter values.
   */
  protected function translateFilterValues($reset = FALSE) {
    if (!$reset && static::$filterValues) {
      return static::$filterValues;
    }

    $filter_values = [];
    $filters = $this->translateFilters();
    $request = $this->getRequest();
    $session_filters = $request->getSession()->get('texts_translate_filter', []);
    foreach ($filters as $key => $filter) {
      $filter_values[$key] = $filter['default'];
      // Let the filter defaults be overwritten by parameters in the URL.
      if ($request->query->has($key)) {
        // Only allow this value if it was among the options, or
        // if there were no fixed options to filter for.
        $value = $request->query->get($key);
        if (!isset($filter['options']) || isset($filter['options'][$value])) {
          $filter_values[$key] = $value;
        }
      }
      elseif (isset($session_filters[$key])) {
        // Only allow this value if it was among the options, or
        // if there were no fixed options to filter for.
        if (!isset($filter['options']) || isset($filter['options'][$session_filters[$key]])) {
          $filter_values[$key] = $session_filters[$key];
        }
      }
    }

    return static::$filterValues = $filter_values;
  }

  /**
   * Lists locale translation filters that can be applied.
   */
  protected function translateFilters() {
    $filters = [];

    // Get all languages, except English.
    $this->languageManager->reset();
    $languages = $this->languageManager->getLanguages();
    $language_options = [];
    $language_options['all'] = $this->t('All');
    foreach ($languages as $langcode => $language) {
      $language_options[$langcode] = $language->getName();
    }

    $filters['string'] = [
      'title' => $this->t('Text or key contains'),
      'description' => $this->t('Leave blank to show all strings. The search is case sensitive.'),
      'default' => '',
    ];

    $filters['langcode'] = [
      'title' => $this->t('Translation language'),
      'options' => $language_options,
      'default' => 'all',
    ];

    $contexts = ['all' => $this->t('All contexts')];
    $contexts = array_merge($contexts, TextsContext::getStaticContextOptions());

    $filters['context'] = [
      'title' => $this->t('Context'),
      'options' => $contexts,
      'default' => 'all',
    ];

    return $filters;
  }

  /**
   * @param $filter
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   */
  protected function getDisplayedLanguages($filter = 'all') {
    $options = [];

    $this->languageManager->reset();
    $languages = $this->languageManager->getLanguages();

    foreach ($languages as $language) {
      if ($filter === 'all' || $language->getId() === $filter) {
        $options[$language->getId()] = $language;
      }
    }

    $disabled_languages = $this->configFactory->get('texts.settings')->get('disabled_languages') ?? [];
    $disabled_codes = array_filter($disabled_languages, function ($value) {
      return $value !== '0';
    });
    $options = array_diff_key($options, $disabled_codes);

    return $options;

  }

  /**
   * Builds a string search query and returns an array of string objects.
   *
   * @return \Drupal\texts\TextsInterface[]
   *   Array of \Drupal\texts\Entity\Texts objects.
   */
  protected function loadExistingStrings(array $ids) {
    /** @var \Drupal\texts\TextsInterface[] $results */
    $results = $this->stringTranslationStorage->loadMultiple($ids);
    return $results;
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  protected function getOperations(EntityInterface $entity) {
    $listBuilder = new EntityListBuilder($entity->getEntityType(), $this->stringTranslationStorage);
    $operations = $listBuilder->getOperations($entity);
    return $operations;
  }

}
