<?php

namespace Drupal\texts\Form\Overview;

use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a translation edit form.
 *
 * @internal
 */
class TranslationEditForm extends TranslationFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'texts_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filter_values = $this->translateFilterValues();
    $langcode = $filter_values['langcode'];

    $this->languageManager->reset();
    $languages = $this->languageManager->getLanguages();

    $shown_languages = $this->getDisplayedLanguages($langcode);

    $langname = isset($langcode) && !empty($languages[$langcode]) ? $languages[$langcode]->getName() : "- None -";

    $headers = [
      $this->t('Translation Key'),
    ];

    foreach ($shown_languages as $shown_language) {
      $headers[$shown_language->getId()] = $shown_language->getName();
    }

    $headers['operation'] = $this->t('Operations');

    $form['#attached']['library'][] = 'texts/admin';

    $form['langcode'] = [
      '#type' => 'value',
      '#value' => $filter_values['langcode'],
    ];

    $form['strings'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#language' => $langname,
      '#header' => $headers,
      '#empty' => $this->t('No strings available.'),
      '#attributes' => ['class' => ['locale-translate-edit-table']],
    ];

    if (isset($langcode)) {
      $strings = $this->translateFilterLoadStrings();
      foreach ($strings as $string) {

        $form['strings'][$string->id()]['key'] = [
          '#type' => 'item',
          '#title' => $this->t('Translation key'),
          '#title_display' => 'invisible',
          '#plain_text' => $string->getTranslationKey(),
        ];

        if (!empty($string->getContext())) {
          $form['strings'][$string->id()]['key'][] = [
            '#type' => 'inline_template',
            '#template' => '<br><small>{{ context_title }}: <span lang="en">{{ context }}</span></small>',
            '#context' => [
              'context_title' => $this->t('In Context'),
              'context' => $string->getContext(),
            ],
          ];
        }

        foreach ($shown_languages as $language) {
          // Approximate the number of rows to use in the default textarea.
          $rows = min(ceil(str_word_count($string->getTranslationText()) + 0.01 / 12), 10);

          $translation_text = '';
          if ($string->hasTranslation($language->getId())) {
            $translated_entity = $string->getTranslation($language->getId());
            $translation_text = $translated_entity->getTranslationText();
          }

          $is_plural = $string->isPlural();

          if (!$is_plural) {

            $form['strings'][$string->id()]['translation_' . $language->getId()] = [
              '#type' => 'textarea',
              '#title' => $language->getName(),
              '#title_display' => 'invisible',
              '#rows' => $rows,
              '#default_value' => $translation_text,
              '#attributes' => ['lang' => $langcode],
            ];
          }
          else {

            $plural_texts = $this->getPlurals($translation_text);

            $form['strings'][$string->id()]['translation_' . $language->getId()]['singular'] = [
              '#type' => 'textarea',
              '#title' => $this->t('Singular form'),
              '#default_value' => $plural_texts[0],
              '#rows' => $rows,
            ];

            $form['strings'][$string->id()]['translation_' . $language->getId()]['plural'] = [
              '#type' => 'textarea',
              '#title' => $this->t('Plural form'),
              '#default_value' => $plural_texts[1],
              '#rows' => $rows,
            ];
          }

        }

        $form['strings'][$string->id()]['operation'] = [
          '#type' => 'operations',
          '#links' => $this->getOperations($string),
        ];

      }
      if (count(Element::children($form['strings']))) {
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save translations'),
        ];
      }
    }
    $form['pager']['#type'] = 'pager';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('strings') as $id => $translations) {
      foreach ($translations as $key => $value) {
        if (strpos($key, 'translation_') === 0) {

          if (is_array($value)) {
            foreach ($value as $plural => $inner_value) {
              if (!locale_string_is_safe($inner_value)) {
                $form_state->setErrorByName(
                  "strings][$id][$key][$plural",
                  $this->t('The submitted string contains disallowed HTML: %string', ['%string' => $inner_value])
                );
              }
            }
          }
          else {
            if (!locale_string_is_safe($value)) {
              $form_state->setErrorByName(
                "strings][$id][$key",
                $this->t('The submitted string contains disallowed HTML: %string', ['%string' => $value])
              );
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Preload all translations for strings in the form.
    $ids = array_keys($form_state->getValue('strings'));
    $existing_translation_strings = $this->loadExistingStrings($ids);

    foreach ($form_state->getValue('strings') as $id => $new_translations) {

      foreach ($new_translations as $key => $new_translation) {
        if (strpos($key, 'translation_') === 0) {
          $langcode = substr($key, strlen('translation_'));

          if ($existing_translation_strings[$id]->isPlural()) {
            $new_translation = $this->savePlural($new_translation);
          }

          // Translation already exists.
          if ($existing_translation_strings[$id]->hasTranslation($langcode) &&
            $existing_translation_strings[$id]->getTranslationText() !== $new_translation) {
            $translated_entity = $existing_translation_strings[$id]->getTranslation($langcode);
            $translated_entity->setTranslationText($new_translation);
            $translated_entity->save();
          }

          // No translation yet.
          if (!$existing_translation_strings[$id]->hasTranslation($langcode) && !empty($new_translation)) {
            $values = $existing_translation_strings[$id]->toArray();
            $values['translation'] = $new_translation;
            $translation = $existing_translation_strings[$id]->addTranslation($langcode, $values);
            $translation->save();
          }

          // Remove translation
          if ($existing_translation_strings[$id]->hasTranslation($langcode) && empty($new_translation)) {
            // Delete translations but skip base language.
            if ($existing_translation_strings[$id]->language()->getId() !== $langcode) {
              $existing_translation_strings[$id]->removeTranslation($langcode);
              $existing_translation_strings[$id]->save();
            }
          }

        }
      }
    }

    $this->messenger()->addStatus($this->t('The strings have been saved.'));

    // Keep the user on the current pager page.
    $page = $this->getRequest()->query->get('page');
    if (isset($page)) {
      $form_state->setRedirect(
        'entity.texts.collection',
        [],
        ['page' => $page]
      );
    }
  }

  public function getPlurals($string) {
    $result = [$string, $string];
    if (strpos($string, PoItem::DELIMITER) !== FALSE) {
      $result = explode(PoItem::DELIMITER, $string);
    }
    return $result;
  }

  public function savePlural($elements) {
    return implode(PoItem::DELIMITER, $elements);
  }

}
