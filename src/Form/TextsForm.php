<?php

namespace Drupal\texts\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the string translation entity edit forms.
 */
class TextsForm extends ContentEntityForm {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RendererInterface $renderer) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Approximate the number of rows to use in the default textarea.
    $default_value = $form['translation']['widget'][0]['value']['#default_value'] ?? ' ';
    $rows = min(ceil((str_word_count($default_value) + 0.01) / 12), 10);

    $form['translation']['widget'][0]['value']['#rows'] = $rows;

    $form['translation']['#states'] = [
      'visible' => [
        ':input[name="plural[value]"]' => ['checked' => FALSE],
      ],
    ];

    $form['translation']['widget'][0]['value']['#states'] = [
      'required' => [
        ':input[name="plural[value]"]' => ['checked' => FALSE],
      ],
    ];

    $form['singular_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Singular form'),
      '#default_value' => '',

      '#states' => [
        'visible' => [
          ':input[name="plural[value]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="plural[value]"]' => ['checked' => TRUE],
        ],
      ],
      '#rows' => $rows,
      '#weight' => 70,
    ];

    $form['plural_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Plural form'),
      '#default_value' => '',
      '#states' => [
        'visible' => [
          ':input[name="plural[value]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="plural[value]"]' => ['checked' => TRUE],
        ],
      ],
      '#rows' => $rows,
      '#weight' => 71,
    ];

    $form['current_language'] = [
      '#type' => 'item',
      '#title' => $this->t('Language'),
      '#markup' => $this->entity->language()->getName(),
      '#weight' => 90,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'), 'edit-form')->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New string translation %label has been created.', $message_arguments));
      $this->logger('texts')->notice('Created new string translation %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The string translation %label has been updated.', $message_arguments));
      $this->logger('texts')->notice('Updated new string translation %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.texts.collection');

    return $result;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('plural')['value'] === 1) {
      // Temporarily store all form errors.
      $form_errors = $form_state->getErrors();
      // Clear the form errors.
      $form_state->clearErrors();

      // Remove the field_mobile form error.
      unset($form_errors['translation][0][value']);

      // Now loop through and re-apply the remaining form error messages.
      foreach ($form_errors as $name => $error_message) {
        $form_state->setErrorByName($name, $error_message);
      }

      $translation_value = $this->savePlural($form_state->getValue('singular_text'), $form_state->getValue('plural_text'));
      $form_state->setValueForElement($form['translation']['widget'][0], ['value' => $translation_value]);
    }

    return parent::validateForm($form, $form_state);

  }

  public function savePlural($singular, $plural) {
    return implode(PoItem::DELIMITER, [$singular, $plural]);
  }

}
