<?php

namespace Drupal\texts\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for this module.
 */
class SettingsForm extends ConfigFormBase {

  protected $languageManager;

  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    LanguageManagerInterface $language_manager
  ) {
    parent::__construct($config_factory, $typedConfigManager);

    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'texts_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['texts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE);
    $options = array_map(function (LanguageInterface $language) {
      return $language->getName();
    }, $languages);

    $form['disabled_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages hidden in the overview'),
      '#options' => $options,
      '#default_value' => $this->config('texts.settings')->get('disabled_languages') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('texts.settings')
      ->set('disabled_languages', $form_state->getValue('disabled_languages'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
