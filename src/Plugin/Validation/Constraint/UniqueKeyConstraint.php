<?php

namespace Drupal\texts\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ensures translation key uniqueness.
 *
 * @Constraint(
 *   id = "UniqueKey",
 *   label = @Translation("The key of a translations string.", context = "Validation")
 * )
 */
class UniqueKeyConstraint extends Constraint {

  public $message = 'The translation key %key is already in use and must be unique.';

}
