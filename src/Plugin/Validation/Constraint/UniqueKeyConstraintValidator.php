<?php

namespace Drupal\texts\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueKeyConstraint constraint.
 */
class UniqueKeyConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$item = $items->first()) {
      return;
    }

    $translation_key = $item->value;
    $context = $items->getEntity()->getContext() ?? 'default';
    if (isset($translation_key) && $translation_key !== '') {
      $key_exists = (bool) \Drupal::entityQuery('texts')
        ->accessCheck(FALSE)
        ->condition('key', $translation_key)
        ->condition('context', $context)
        ->condition('id', (int) $items->getEntity()->id(), '<>')
        ->range(0, 1)
        ->count()
        ->execute();

      if ($key_exists) {
        /** @var \Drupal\texts\Plugin\Validation\Constraint\UniqueKeyConstraint $constraint */
        $this->context->buildViolation($constraint->message)
          ->setParameter('%key', $this->formatValue($translation_key))
          ->addViolation();
      }
    }
  }

}
