<?php

namespace Drupal\texts;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a string translation entity type.
 */
interface TextsInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the context value.
   *
   * @return string
   */
  public function getContext() : string;

  /**
   * Sets the context value.
   *
   * @param string $context
   *
   * @return \Drupal\texts\TextsInterface
   *   The called string translation entity.
   */
  public function setContext(string $context) : TextsInterface;

  /**
   * Gets the translation key.
   *
   * @return string
   */
  public function getTranslationKey(): string;

  /**
   * Sets the translation key.
   *
   * @param string $key
   *
   * @return \Drupal\texts\TextsInterface
   *   The called string translation entity.
   */
  public function setTranslationKey(string $key) : TextsInterface;

  /**
   * Determine if plural is used.
   *
   * @return bool
   */
  public function isPlural() : bool;

  /**
   * Sets the plural value.
   *
   * @param bool $plural
   *
   * @return \Drupal\texts\TextsInterface
   *   The called string translation entity.
   */
  public function setPlural(bool $plural) : TextsInterface;

  /**
   * Gets the translation text.
   *
   * @return string
   */
  public function getTranslationText() : string;

  /**
   * Sets the translation text.
   *
   * @param string $text
   *
   * @return \Drupal\texts\TextsInterface
   *   The called string translation entity.
   */
  public function setTranslationText(string $text): TextsInterface;

  /**
   * Gets the string translation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the string translation.
   */
  public function getCreatedTime();

  /**
   * Sets the string translation creation timestamp.
   *
   * @param int $timestamp
   *   The string translation creation timestamp.
   *
   * @return \Drupal\texts\TextsInterface
   *   The called string translation entity.
   */
  public function setCreatedTime($timestamp);

}
