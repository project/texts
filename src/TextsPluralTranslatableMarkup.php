<?php

namespace Drupal\texts;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;

class TextsPluralTranslatableMarkup extends PluralTranslatableMarkup {

  /**
   * Renders the object as a string.
   *
   * @return string
   *   The translated string.
   */
  public function render() {
    if (!isset($this->translatedMarkup)) {
      $this->translatedMarkup = $this->string;
    }
    return parent::render();
  }

}
