<?php

namespace Drupal\texts;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

class TextsStorage extends SqlContentEntityStorage implements TextsStorageInterface {

  public function loadByKey($key, $context = 'default'): TextsInterface|null {
    $texts = $this->loadByProperties(['key' => $key, 'context' => $context]);
    $text = reset($texts);

    return $text instanceof TextsInterface ? $text : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByKey(array $keys = []): array|null {

    $texts_ids = $this->loadByProperties(['key' => $keys]);
    $key_sorted = [];

    if ($texts_ids) {
      /** @var \Drupal\texts\Entity\Texts $text */
      foreach ($texts_ids as $text) {
        $context = $text->getContext();
        $key = $text->getTranslationKey();
        $arrayKey = $context . '.' . $key;
        $key_sorted[$arrayKey] = $text;
      }
    }

    return $key_sorted;
  }

}
