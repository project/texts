<?php

namespace Drupal\texts\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Return response for manual check translations.
 */
class TextsController extends ControllerBase {

  /**
   * Shows the string locale search screen.
   *
   * @return array
   *   The render array for the string search screen.
   */
  public function translatePage() {
    return [
      'filter' => $this->formBuilder()->getForm('Drupal\texts\Form\Overview\TranslationFilterForm'),
      'form' => $this->formBuilder()->getForm('Drupal\texts\Form\Overview\TranslationEditForm'),
    ];
  }

}
