<?php

namespace Drupal\texts;

interface TextsTranslatorInterface {

  /**
   * Gets a texts and its translation by its key.
   *
   * @param string $key
   *   The translation key.
   * @param array $args
   *   (optional) An associative array of replacements to make after
   *   translation. Based on the first character of the key, the value is
   *   escaped and/or themed. See
   *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
   *   details.
   * @param array $options
   *   (optional) An associative array of additional options, with the following
   *   elements:
   *   - 'default_translation' (defaults to the current language): A language code, to
   *     translate to a language other than what is used to display the page.
   *   - 'context' (defaults to the 'default' context): The context the source
   *     string belongs to.
   * @param mixed|null $text
   *   (optional) Preloaded texts entity.
   *
   * @return string
   */
  public function trans(string $key, array $args = [], array $options = [], mixed $text = NULL): string;

  /**
   * Formats a plural text including placeholders.
   *
   * @param int $count
   *   The item count to display.
   * @param string $key
   *   The translation key.
   * @param string $singular
   *   The string for the singular case. Make sure it is clear this is singular,
   *   to ease translation (e.g. use '1 new comment' instead of '1 new'). Do not
   *   use @count in the singular string.
   * @param string $plural
   *   The string for the plural case. Make sure it is clear this is plural, to
   *   ease translation. Use @count in place of the item count, as in
   *   '@count new comments'.
   * @param array $args
   *   (optional) An associative array of replacements to make after
   *   translation. Based on the first character of the key, the value is
   *   escaped and/or themed. See
   *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
   *   details.
   * @param array $options
   *   (optional) An associative array of additional options, with the following
   *   elements:
   *   - 'context' (defaults to the 'default' context): The context the source
   *     string belongs to.
   *
   * @return string
   */
  public function formatPlural(
    int $count,
    string $key,
    string $singular = '',
    string $plural = '',
    array $args = [],
    array $options = []
  ): string;

  /**
   * Translate multiple texts at once.
   *
   * @param array $translations
   *   Provide an array od translations.
   * @param array $cache_tags
   *   (optional) An array of cache tags to add.
   *
   * @return \Drupal\texts\TextsInterface[]
   */
  public function translateMultiple(array $translations, &$cache_tags = []): array;

}
