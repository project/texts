<?php

namespace Drupal\texts;

trait TextsTranslationTrait {


  protected $textTranslator;

  protected function getTexts(string $key, array $args = [], array $options = []): string {
    return $this->getTranslator()->trans($key, $args, $options);
  }

  /**
   * Formats a string containing a count of items.
   *
   * @see \Drupal\Core\StringTranslation\TranslationInterface::formatPlural()
   */
  protected function getTextsPlural($count, $key, $singular, $plural, array $args = [], array $options = []) {
    return $this->getTranslator()->formatPlural($count, $key, $singular, $plural, $args, $options);
  }

  /**
   * Load the translator.
   *
   * @return \Drupal\texts\TextsTranslatorInterface
   */
  protected function getTranslator() {
    if (!$this->textTranslator) {
      $this->textTranslator = \Drupal::service('texts.translator');
    }

    return $this->textTranslator;
  }

}
