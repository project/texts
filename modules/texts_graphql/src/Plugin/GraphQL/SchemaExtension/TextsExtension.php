<?php

namespace Drupal\texts_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\texts\TextsTranslator;
use Drupal\texts\TextsTranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds texts data to the GraphQL Compose GraphQL API.
 *
 * @SchemaExtension(
 *   id = "texts",
 *   name = "Texts Schema Extension",
 *   description = "Texts Route Extension.",
 *   schema = "core_composable"
 * )
 */
class TextsExtension extends SdlSchemaExtensionPluginBase {

  /**
   * @var \Drupal\texts\TextsTranslatorInterface
   */
  protected $translator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('texts.translator')
    );
  }

  /**
   * TextsExtension constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\texts\TextsTranslatorInterface $translator
   *   The module handler service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    TextsTranslatorInterface $translator,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);
    $this->translator = $translator;
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addTypeResolver('TextsInterface', function ($value) {
      if (isset($value['singular'])) {
        return 'TextsPlural';
      }
      return 'TextsDefault';
    });

    $query_types = ['Query', 'TextsLoader'];

    $registry->addFieldResolver(
      'Query',
      'textsLoader',
      $builder->callback(function () {
        return TRUE;
      }
      )
    );

    foreach ($query_types as $type) {
      $registry->addFieldResolver(
        $type,
        'getText',
        $builder->callback(function ($value, $args, ResolveContext $context) {
          $default_translation = !empty($args['default']) ? $args['default'] : '';
          $translation_context = !empty($args['context']) ? $args['context'] : 'default';

          // Add a cache context per texts context.
          $context->addCacheTags(['texts_context:' . $translation_context]);

          $translation = $this->translator->trans(
            $args['key'],
            [],
            [
              'default_translation' => $default_translation,
              'context' => $translation_context,
            ]
          );

          if (TextsTranslator::isPlural($translation)) {
            $split = TextsTranslator::getPluralsParts($translation);
            return $split[0];
          }

          return $translation;

        })
      );

      $registry->addFieldResolver(
        $type,
        'getTextPlural',
        $builder->callback(function ($value, $args, ResolveContext $context) {
          $singular = !empty($args['singular']) ? $args['singular'] : '';
          $plural = !empty($args['plural']) ? $args['plural'] : '';
          $translation_context = !empty($args['context']) ? $args['context'] : 'default';

          // Add a cache context per texts context.
          $context->addCacheTags(['texts_context:' . $translation_context]);

          $result = $this->translator->trans(
            $args['key'],
            [],
            [
              'default_translation' => TextsTranslator::getPluralMerged(
                [$singular, $plural]
              ),
              'context' => $translation_context,
            ]
          );

          $translation = TextsTranslator::getPluralsParts($result);

          return [
            'singular' => $translation[0],
            'plural' => $translation[1],
            'context' => $translation_context,
          ];
        })
      );

      $registry->addFieldResolver(
        $type,
        'getTextMultiple',
        $builder->callback(function ($value, $args, ResolveContext $context) {
          $cache_tags = [];
          /** @var \Drupal\texts\Entity\Texts[] $translations */
          $translations = $this->translator->translateMultiple($args['texts'], $cache_tags);

          // Add a cache context for each used texts' context.
          $context->addCacheTags(array_keys($cache_tags));

          $result = [];

          foreach ($translations as $arrayKey => $translation) {
            [$textContext, $key] = explode('.', $arrayKey);

            // If we have a plural string, react on that.
            if (TextsTranslator::isPlural($translation)) {
              $split = TextsTranslator::getPluralsParts($translation);
              $resolved = [
                'singular' => $split[0],
                'plural' => $split[1],
              ];
            }
            else {
              $resolved = [
                'default' => $translation,
              ];
            }

            $resolved['key'] = $key;
            if ($textContext !== 'default') {
              $resolved['context'] = $textContext;
            }
            $result[] = $resolved;
          }

          return $result;
        })
      );
    }

  }

}
